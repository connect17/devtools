# DevTools
## Scripts and tools to help development

### Installation

In your project's composer.json, add these lines:

    require-dev": {
        "connect17/devtools": "^0.1",
        ...
    },
    "repositories": [
        ...,
        {
            "url": "git@bitbucket.org:connect17/devtools.git",
            "type": "git"
        },
        ...
    ]

### License

[BundleLaundry](http://www.bundlelaundry.com/) All Rights Reserved @2017