#!/bin/bash
BASE="$(cd "$(dirname "$0")"; pwd)"
source "$BASE/functions.sh"

# defaults
branch="master"

while getopts b:e:v:mdsh opt; do
  case $opt in
    b)
      branch=$OPTARG
    ;;
    e)
      deploy_env=$OPTARG
    ;;
    v)
      deploy_ver=$OPTARG
    ;;
    h)
       cat << EOM
  usage: $0 [-b base_branch_name] [-e deploy_env] [-v deploy_ver]
  options:
    -b sets the base branch name (default: $branch)
    -e deploy environment (staging of production)
    -v deploy version (version tag to be deployed)

    This script deploy a verion of a branch to an environment with collaberation of Gitlab ci scripts.
EOM
      exit
    ;;
  esac
done

fn_switch_branch $branch
# fn_pull_all $branch
fn_fetch_tags

if [ -z $deploy_env ]
then
  deploy_env="staging"
fi

[ "$deploy_env" = "staging" ] || [ "$deploy_env" = "production" ] || fn_abort "Environment '$deploy_env' does not exist."

if [ -z $deploy_ver ]
then
  current_version=$(fn_current_version)
  deploy_ver="v$current_version"
else
  deploy_ver="v$deploy_ver"
fi

fn_tag_exists $deploy_ver || fn_abort "Release tag '$deploy_ver' does not exists."

echo "Deploy '$deploy_ver' to '$deploy_env'... "
fn_deploy $branch $deploy_ver $deploy_env

echo -n "Pushing to origin... "
git push origin $branch $deploy_env --force &>/dev/null || fn_error "Push to origin failed."
fn_success

echo "all done"
